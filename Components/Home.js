import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Dimensions,
  Text,
  Image,
  TouchableOpacity,
} from 'react-native';
//import faker from 'faker';
import {
  RecyclerListView,
  DataProvider,
  LayoutProvider,
} from 'recyclerlistview';
import {
  Container,
  Header,
  Content,
  Button,
  Left,
  Body,
  Right,
  Icon,
  Title,
  Form,
  Item,
  Input,
  Label,
  List,
  ListItem,
} from 'native-base';

const SCREEN_WIDTH = Dimensions.get('window').width;

export default class App extends Component {
  constructor(props) {
    super(props);
    var i;
    const fakeData = [];
    for (i = 0; i < 100; i += 1) {
      fakeData.push({
        type: 'NORMAL',
        item: {
          id: i,
          image:'./image/important-person_318-10744.jpg',
          name: 'test',
          description: 'Welcome to my first react-native project',
        },
      });
    }
    this.state = {
      list: new DataProvider((r1, r2) => r1 !== r2).cloneWithRows(fakeData),
    };

    this.layoutProvider = new LayoutProvider(
      i => {
        return this.state.list.getDataForIndex(i).type;
      },
      (type, dim) => {
        switch (type) {
          case 'NORMAL':
            dim.width = SCREEN_WIDTH;
            dim.height = 100;
            break;
          default:
            dim.width = 0;
            dim.height = 0;
            break;
        }
      }
    );
  }

  rowRenderer = (type, data) => {
    const { image, name, description } = data.item;
    return (
      <View style={styles.listItem}>
        <Image style={styles.image} source={require('./image/important-person_318-10744.jpg')}/>
        <View style={styles.body}>
          <Text style={styles.name}>test</Text>
          <Text style={styles.description}>Welcome to my first react-native project</Text>
        </View>
      </View>
    );
  };

  render() {
    return (
      <Container>
        <Header>
          <Body />
          <Right>
            <Icon
              name="menu"
              onPress={() => this.props.navigation.openDrawer()}
            />
          </Right>
        </Header>
        <View style={styles.container}>
          <RecyclerListView
            style={{ flex: 1 }}
            rowRenderer={this.rowRenderer}
            dataProvider={this.state.list}
            layoutProvider={this.layoutProvider}
          />
        </View>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFF',
    minHeight: 1,
    minWidth: 1,
  },
  body: {
    marginLeft: 10,
    marginRight: 10,
    maxWidth: SCREEN_WIDTH - (80 + 10 + 20),
  },
  image: {
    height: 80,
    width: 80,
  },
  name: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  description: {
    fontSize: 14,
    opacity: 0.5,
  },
  listItem: {
    flexDirection: 'row',
    margin: 10,
  },
});
